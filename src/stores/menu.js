// import { defineStore } from 'pinia'

// export const useMenuStore = defineStore('menu', {
//   state: () => ({
//     menus: {
//       submenu1: ['Item 1-1', 'Item 1-2', 'Item 1-3'],
//       submenu2: ['Item 2-1', 'Item 2-2', 'Item 2-3'],
//       submenu3: ['Item 3-1', 'Item 3-2', 'Item 3-3']
//     },
//     selectedMenu: null,
//     selectedItem: null
//   }),
//   actions: {
//     selectMenu (menu) {
//       this.selectedMenu = menu
//       this.selectedItem = null // Reset the selected item when a menu is selected
//     },
//     selectItem (item) {
//       this.selectedItem = item
//     }
//   },
//   getters: {
//     submenuItems: state => {
//       return state.selectedMenu ? state.menus[state.selectedMenu] : []
//     }
//   }
// })
